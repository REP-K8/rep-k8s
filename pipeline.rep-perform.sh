#!/bin/sh
# This script tags and pushes a docker image from local registry into kubernetes registry on IBM cloud

export BASE_DIR="/home/at062084/DataEngineering/REP/rep-k8s"
cd $BASE_DIR

# !!!!! tagging the docker image for ibmregistry names the regspace !!!!!!!!!!!!!!

export APPS="influxdb grafana k6"
[ $# -ge 1 ] && APPS=$1

export ENV=""
[ $# -ge 2 ] && ENV=$2

export DRY_RUN=0
if [ $# -ge 3 ]
then
        DRY_RUN=1
        echo "Dryrun mode ..."
fi


for APP in $APPS
do
  echo ""
  export APP_DIR=$BASE_DIR/../rep-perfmon/$APP
  echo "Building $APP in $APP_DIR"
  cd $APP_DIR
  [ $DRY_RUN -eq 0 ] && ./build.sh
 
  cd $BASE_DIR

  # commands to build and deploy a docker image of itg-rshiny to kubernetes
  export DKR_PRJ="rep-$APP"
  export GIT_VER="latest"
  export DKR_TAG="$DKR_PRJ:$GIT_VER"; echo $DKR_TAG
  export K8S_PRJ=$DKR_PRJ
  [ ! -z "$ENV" ] && K8S_PRJ="${DKR_PRJ}-${ENV}"
  echo "$DRR_PRJ $DKR_TAG $K8S_PRJ"

  # tag image and upload to IBM cloud registry
  export CMD="docker tag  $DKR_TAG $IBM_REG/$IBM_REG_SPACE/$DKR_TAG"
  echo $CMD
  [ $DRY_RUN -eq 0 ] && $CMD

  export CMD="docker push $IBM_REG/$IBM_REG_SPACE/$DKR_TAG"
  echo $CMD
  [ $DRY_RUN -eq 0 ] && $CMD

  ibmcloud cr images | grep $DKR_PRJ

  if [ ! $APP == "k6" ]
  then

    if [ -r secrets.$K8S_PRJ.yaml ]
    then
      export CMD="kubectl apply -f secrets.$K8S_PRJ.yaml"
      echo $CMD
      [ $DRY_RUN -eq 0 ] && $CMD
    fi  
	 
    if [ -r configmap.$K8S_PRJ.yaml ]
    then
      export CMD="kubectl apply -f configmap.$K8S_PRJ.yaml"
      echo $CMD
      [ $DRY_RUN -eq 0 ] && $CMD
    fi  
	 
    if [ -r deployment.$K8S_PRJ.yaml ]
    then
      export CMD="kubectl delete deployment $K8S_PRJ -n repnamespace"
      echo $CMD
      [ $DRY_RUN -eq 0 ] && $CMD
      sleep 3
      export CMD="kubectl apply -f deployment.$K8S_PRJ.yaml"
      echo $CMD
      [ $DRY_RUN -eq 0 ] && $CMD
    fi

    if [ -r daemonset.$K8S_PRJ.yaml ]
    then
      export CMD="kubectl delete daemonset $K8S_PRJ -n repnamespace"
      echo $CMD
      [ $DRY_RUN -eq 0 ] && $CMD
      sleep 3
      export CMD="kubectl apply -f daemonset.$K8S_PRJ.yaml"
      echo $CMD
      [ $DRY_RUN -eq 0 ] && $CMD
    fi

    if [ -r service.$K8S_PRJ.yaml ]
    then
      export CMD="kubectl apply -f service.$K8S_PRJ.yaml"
      echo $CMD
      [ $DRY_RUN -eq 0 ] && $CMD
    fi

    # List current objects
    echo "--- Deployments:"
    kubectl get deployments -n repnamespace 
    echo "--- Daemonsets:"
    kubectl get daemonsets -n repnamespace 
    echo "--- Nodes:"
    kubectl get nodes -n repnamespace 
    echo "--- Pods:"
    kubectl get pods -n repnamespace 
    echo "--- Secrets:"
    kubectl get secrets -n repnamespace 
    echo "--- ConfigMaps:"
    kubectl get configmaps -n repnamespace 
  fi
done

